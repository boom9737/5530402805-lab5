package kitteerapakorn.attaphon.lab5;

public class Player extends kitteerapakorn.attaphon.lab4.Player {

	String language;

	public Player(String name, int dDay, int dMonth, int dYear, double weight,
			double height) {
		super(name, dDay, dMonth, dYear, weight, height);
	}

	public Player(String name, int dDay, int dMonth, int dYear, double weight,
			double height, String language) {
		super(name, dDay, dMonth, dYear, weight, height);
	}

	public void speak() {
		System.out.println("Speak " + language);

	}

}
