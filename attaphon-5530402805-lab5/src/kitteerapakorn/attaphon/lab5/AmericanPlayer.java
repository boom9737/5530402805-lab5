package kitteerapakorn.attaphon.lab5;

public class AmericanPlayer extends Player {

	private String facebookAcct;

	public AmericanPlayer(String name, int dDay, int dMonth, int dYear,
			double weight, double height) {
		super(name, dDay, dMonth, dYear, weight, height);

	}

	public AmericanPlayer(String name, int dDay, int dMonth, int dYear,
			double weight, double height, String Facebook) {
		super(name, dDay, dMonth, dYear, weight, height);

		facebookAcct = Facebook;

	}

	public String getFacebookAcct() {
		return facebookAcct;
	}

	public void setFacebookAcct(String NewFacebook) {
		facebookAcct = NewFacebook;
	}
	
	public void speak() {
		language = "English";
		System.out.println("Speak " + language);
	}
}
