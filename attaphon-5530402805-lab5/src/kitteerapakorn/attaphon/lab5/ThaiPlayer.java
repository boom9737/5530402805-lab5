package kitteerapakorn.attaphon.lab5;

public class ThaiPlayer extends Player {

	private String award;

	public ThaiPlayer(String name, int dDay, int dMonth, int dYear,
			double weight, double height) {
		super(name, dDay, dMonth, dYear, weight, height);

	}
	
	public ThaiPlayer(String name, int dDay, int dMonth, int dYear,
			double weight, double height, String award) {
		super(name, dDay, dMonth, dYear, weight, height);
		
		this.award = award;

	}


	public String getAward() {
		return award;
	}

	public void setAward(String newAward) {
		award = newAward;
	}

	public void speak() {

		System.out.println("�ٴ������");

	}
	
	public String toString()
	{
		
		
		return "Player [" + this.getName() + " is " + 
					this.Year() + " year old, with weight is " + weight +
					" and height is " + height + "]\n" + "[ nationality = Thai]";
					
	
	}
	

}
